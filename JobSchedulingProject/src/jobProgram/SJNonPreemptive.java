package jobProgram;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

/*
 * This class represents the Shortest Job Non-Preemptive Algorithm
 * 
 * This algorithm chooses the job with the shortest burst time available in the queue, 
 * and focuses on it until its complete before moving to another job. 
 */
public class SJNonPreemptive extends Algorithm {

	private ArrayList<Job> jobs;
	private int time;
	private int maxTime;
	private Job selectedJob;
	private int numJobsDone;
	
	private Queue<Job> jobQueue;
	
	public SJNonPreemptive(ArrayList<Job> jobs) {
		this.jobs = jobs;
		this.time = 0;
		this.maxTime = -1;
	}

	@Override
	public void run() {
		jobQueue = new LinkedList<Job>();
		time = 0;
		selectedJob = null;
		if (maxTime==-1) {
			numJobsDone = 0;
			boolean running = true;
			while (running) {
				for (Job j: jobs) {
					if (j.getInitTime()==time)
						jobQueue.add(j);
				}
				if (selectedJob==null) {
					selectedJob = jobQueue.peek();
					for (Job j: jobQueue) {
						if (selectedJob.getBurst()>j.getBurst())
							selectedJob = j;
					}
				}
				selectedJob.run();
				time++;
				if (selectedJob.isCompleted()) {
					selectedJob.setTimeCompleted(time);
					jobQueue.remove(selectedJob);
					System.out.println("At time="+time+": "+selectedJob);
					selectedJob = null;
					numJobsDone++;
				}
				else
					System.out.println("At time="+time+": "+selectedJob);
				if (numJobsDone==jobs.size())
					running = false;
			}
		}
		else {
			while (time<maxTime) {
				for (Job j: jobs) {
					if (j.getInitTime()==time)
						jobQueue.add(j);
				}
				if (selectedJob==null) {
					selectedJob = jobQueue.peek();
					for (Job j: jobQueue) {
						if (selectedJob.getBurst()>j.getBurst())
							selectedJob = j;
					}
				}
				selectedJob.run();
				time++;
				if (selectedJob.isCompleted()) {
					selectedJob.setTimeCompleted(time);
					jobQueue.remove(selectedJob);
					System.out.println("At time="+time+": "+selectedJob);
					selectedJob = null;
				}
				else
					System.out.println("At time="+time+": "+selectedJob);
			}
		}
	}

	@Override
	public void printResult() {
		System.out.println("*RESULTS*");
		for (Job j: jobs) {
			System.out.println(j);
		}
		
	}

	@Override
	public void setMaxTime(int time) {
		this.maxTime = time;
		
	}

}
