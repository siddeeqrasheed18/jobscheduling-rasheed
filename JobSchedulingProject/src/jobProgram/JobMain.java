package jobProgram;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/*
 * This class will facilitate running the different job algorithms.
 * A simple menu will allow the user to select which algorithm to run, as well as add a time limit.
 */
public class JobMain {
	
	private static Algorithm algorithm;

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		boolean menuRunning = true;
		String selection = null;
		System.out.println("Welcome to the Job Scheduling Algorithm Menu.");
		System.out.println("Enter 'Q' for First-Come-First-Serve,\n'N' for Shortest Job Non-Preemptive,\nor 'P' for Shortest Job Preemptive");
		while (menuRunning) {
			selection = in.next().toUpperCase();
			switch (selection) {
				case "Q":
					algorithm = new FCFSQueue(getJobs());
					menuRunning = false;
					break;
				case "N":
					algorithm = new SJNonPreemptive(getJobs());
					menuRunning = false;
					break;
				case "P":
					algorithm = new SJPreemptive(getJobs());
					menuRunning = false;
					break;
				default:
					System.err.println("Please input a correct command.");
			}
		}
		System.out.println("Would you like to set a time limit? (Y/N)");
		menuRunning = true;
		while (menuRunning) {
			selection = in.next().toUpperCase();
			switch (selection) {
				case "Y":
					System.out.println("Please enter a time limit");
					int limit = in.nextInt();
					algorithm.setMaxTime(limit);
					System.out.println("Now running...");
					menuRunning = false;
					break;
				case "N":
					System.out.println("Now Running...");
					menuRunning = false;
					break;
				default:
					System.err.println("Please input a correct command");
			}
		}
		in.close();
		algorithm.run();
		algorithm.printResult();
	}	

	/*
	 * This method reads the jobs from JobsToRun.txt and returns an appropriate ArrayList of jobs.
	 * Helper method: processJobLine()
	 */
	public static ArrayList<Job> getJobs() {
		ArrayList<Job> jobs = new ArrayList<Job>();
		String filepath = "JobsToRun.txt";
		Scanner fileIn = null;
		try {
			fileIn = new Scanner(new File(filepath));
			while (fileIn.hasNextLine()) {
				jobs.add(processJobLine(fileIn.nextLine()));
			}
		}
		catch (FileNotFoundException e ) {
			System.err.println("There is no file found at: "+filepath);
			e.printStackTrace();
		}
		finally {
			if (fileIn != null)
				fileIn.close();
		}
		return jobs;
	}

	/*
	 * Helper method for getJobs()
	 * Takes in a String obtained from a line in JobsToRun and returns an appropriate Job object
	 */
	public static Job processJobLine(String line) {
		Scanner lineScanner = new Scanner(line);
		lineScanner.useDelimiter(",");
		
		String title = lineScanner.next();
		int time = lineScanner.nextInt();
		int burst = lineScanner.nextInt();
		
		return new Job(title,time,burst);
	}
}
