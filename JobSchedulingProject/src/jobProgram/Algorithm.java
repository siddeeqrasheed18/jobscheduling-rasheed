package jobProgram;

import java.util.ArrayList;

/*
 * This is an abstract class to represent a Job Scheduling algorithm.
 * 
 * Every algorithm must have:
 * 	jobs - An ArrayList of jobs to work with
 * 	time - An integer to represent the time; always starts at 0
 * 	maxTime - An integer to represent how long the algorithm is allowed to run. Set to -1 as default to represent no maximum time.
 * 	selectedJob - The Job that the algorithm chooses to run at the time
 * 	run() - A method that implements the algorithm
 * 	printResult() - Prints each job after the algorithm has ran.
 *  setMaxTime(int time) - Used to set the maximum time the algorithm is allowed to run.
 */
public abstract class Algorithm {

	private ArrayList<Job> jobs;
	private int time;
	private int maxTime;
	private Job selectedJob;
	
	public abstract void run();

    public abstract void printResult();
    
    public abstract void setMaxTime(int time);
	
	
	
}
