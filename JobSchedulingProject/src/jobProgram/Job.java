package jobProgram;

/*
 * The job class represents a Job. Each job has it's own name, initTime (time entered), 
 * and burst (CPU Burst, aka time the job takes to complete).
 */
public class Job {
	
	//instance variables
	private String name;
	private int initTime;
	private int burst;
	private int timeRemaining;
	private boolean completed;
	private int timeCompleted;
	
	//Job constructor
	public Job(String name, int time, int burst) {
		this.name = name;
		this.initTime = time;
		this.burst = burst;
		this.timeRemaining = burst;
		this.completed = false;
	}

	/*
	 * toString method
	 * Returns a String formatted with the name, time entered, CPU burst, and completion status
	 */
	public String toString() {
		if (completed)
			return "Name: "+name+", Time Entered: "+initTime+", CPU Burst: "+burst+", Time Completed: "+timeCompleted;
		else
			return "Name: "+name+", Time Entered: "+initTime+", CPU Burst: "+burst+", Time Remaining: "+timeRemaining;

	}
	
	//Get & Set Methods
	public String getName() {
		return name;
	}

	public int getInitTime() {
		return initTime;
	}

	public int getBurst() {
		return burst;
	}

	public int getTimeRemaining() {
		return timeRemaining;
	}

	public boolean isCompleted() {
		return completed;
	}

	public void setTimeCompleted(int timeCompleted) {
		this.timeCompleted = timeCompleted;
	}

	/*
	 * This method simulates the job running for one time interval.
	 */
	public void run() {
		timeRemaining--;
		if (timeRemaining==0)
			completed = true;
	}
}
