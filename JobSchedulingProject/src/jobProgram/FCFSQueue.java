package jobProgram;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

/*
 * This class represents the First Come First Serve scheduling algorithm.
 * This implementation is done using a queue and running the job at the head of the queue until all jobs are complete.
 */
public class FCFSQueue extends Algorithm {

	private ArrayList<Job> jobs;
	private int time;
	private int maxTime;
	private Job selectedJob;
	
	private Queue<Job> jobQueue;
	
	public FCFSQueue(ArrayList<Job> jobs) {
		this.jobs = jobs;
		this.time = 0;
		this.maxTime = -1;
	}
	
	/*
	 * Runs a FCFS implementation
	 */
	@Override
	public void run() {
		jobQueue = new LinkedList<Job>();
		time = 0;
		if (maxTime==-1) {
			boolean running = true;
			while (running) {
				for (Job j: jobs) {
					if (j.getInitTime()==time)
						jobQueue.add(j);
				}
				selectedJob = jobQueue.peek();
				selectedJob.run();
				time++;
				if (selectedJob.isCompleted()) {
					selectedJob.setTimeCompleted(time);
					jobQueue.remove(selectedJob);
				}
				System.out.println("At time="+time+": "+selectedJob);
				if (jobQueue.isEmpty())
					running = false;
			}
		}
		else {
			while (time<maxTime) {
				for (Job j: jobs) {
					if (j.getInitTime()==time)
						jobQueue.add(j);
				}
				selectedJob = jobQueue.peek();
				selectedJob.run();
				time++;
				if (selectedJob.isCompleted()) {
					selectedJob.setTimeCompleted(time);
					jobQueue.remove(selectedJob);
				}
				System.out.println("At time="+time+": "+selectedJob);
			}
		}
	}

	@Override
	public void printResult() {
		System.out.println("*RESULTS*");
		for (Job j: jobs) {
			System.out.println(j);
		}
	}

	@Override
	public void setMaxTime(int time) {
		this.maxTime = time;
		
	}
}
